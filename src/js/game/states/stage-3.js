var game = {};
var crowd = [];
var selected = 'helmet';
var climate = 0;
var bars = [];
var targets = [];

var squad_target;
var nav_flag;
var squadGroup;

var fireCloud;
var fireHalo;
var fireStick;
var watersplash;

var batonSd;
var baton2Sd;
var fireSd;
var gameSd;
var loseSd;
var navHitSd;
var navHit2Sd;
var waterSd;
var winSd;
var omonSd;

var interval;
var timer = 30;
var timerText;

game.makeInterval = function() {
  interval = setInterval(function() {
    timer = timer - 1;
  }, 1000);
}

game.makeSounds = function() {
  batonSd   = game.add.audio('baton');
  baton2Sd  = game.add.audio('baton2');
  fireSd    = game.add.audio('fire');
  fireSd.loop = true;
  gameSd    = game.add.audio('game');
  gameSd.loop = true;
  loseSd    = game.add.audio('lose');
  loseSd.loop = true;
  navHitSd  = game.add.audio('nav_hit');
  navHit2Sd = game.add.audio('nav_hit2');
  waterSd   = game.add.audio('water');
  winSd     = game.add.audio('win');
  winSd.loop = true;
  omonSd = game.add.audio('omon');
}

game.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.game.physics.startSystem(Phaser.Physics.ARCADE);

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'game2'
  );
  
  logo.anchor.setTo(0.5, 0.5);

  game.populate(200, 200, 7, 7);
  game.populate(430, 400, 7, 7);
  game.populate(840, 380, 7, 7);

  game.populate(1046, 236, 7, 7);
  game.makeSounds();
  game.interface();
  game.setTime();

  game.makeInterval();
  var clock = this.game.add.sprite(this.game.world.width - 100, 30, 'clock');
  timerText = game.add.text(this.game.world.width - 180, 40, ('00:' + timer), {
    font: "24px Arial",
    fill: "#ff0044",
    align: "center"
  });

  gameSd.play();

  var bar = this.game.add.sprite(20, 10, 'bar');
  game.generateBar();
};

game.generateBar = function() {
  var base = { x: 60, y: 53 };

  for (var i = 0; i < 5; i++) {
    var bar = this.game.add.sprite(
      base.x + (25 * i),
      base.y,
      'health',
      'progressbar_point_00' + (i + 1) + '.png'
    );
    bars.push(bar)
  }
}

game.win = function() {
  targets.forEach(function(t) {
    t.destroy();
  });
  squadGroup && squadGroup.removeAll(true);
  crowd.forEach(function(group) {
    group.forEach(function(p) {
      p.mute = true;
    })
  });
  var that = this;
  clearTimeout(game.timer);
  setTimeout(function() {
    gameSd.stop();
    that.game.state.start('stage4_logo');
  }, 2000);
  clearInterval(interval);
}

game.loose = function() {
  clearTimeout(game.timer);
  clearInterval(interval);
  timer = 30;
  gameSd.stop();
  this.game.state.start('loose');
}

game.interface = function() {
  var helmet = this.game.add.sprite(450, 640, 'helmet_on');
  helmet.anchor.setTo(0.5, 0.5);
  helmet.inputEnabled = true
  helmet.input.useHandCursor = true;
  helmet.events.onInputDown.add(function() {
    selected = 'helmet';
    helmet.loadTexture('helmet_on', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water', 0);
  }, this);

  var q = game.input.keyboard.addKey(Phaser.Keyboard.Q);
  q.onDown.add(function() {
    selected = 'helmet';
    helmet.loadTexture('helmet_on', 0);
    omon.loadTexture('omon', 0);
  }, this);


  var omon = this.game.add.sprite(610, 640, 'omon');
  omon.anchor.setTo(0.5, 0.5);
  omon.inputEnabled = true
  omon.input.useHandCursor = true;
  omon.events.onInputDown.add(function() {
    selected = 'omon';
    omon.loadTexture('omon_on', 0);
    helmet.loadTexture('helmet', 0);
    water.loadTexture('water', 0);
  }, this);

  var w = game.input.keyboard.addKey(Phaser.Keyboard.W);
  w.onDown.add(function() {
    selected = 'omon';
    omon.loadTexture('omon_on', 0);
    helmet.loadTexture('helmet', 0);
  }, this);

  var water = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'water'
  );
  water.anchor.setTo(0.5, 0.5);
  water.inputEnabled = true
  water.input.useHandCursor = true;
  water.events.onInputDown.add(function() {
    selected = 'water';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water_on', 0);
  }, this);

  var e = game.input.keyboard.addKey(Phaser.Keyboard.E);
  e.onDown.add(function() {
    selected = 'water';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water_on', 0);
  }, this);

  game.add.tween(water).to( { x: 740, y: 640 }, 1500, Phaser.Easing.Bounce.Out, true);

  var gas = this.game.add.sprite(850, 640, 'gas_off');
  gas.anchor.setTo(0.5, 0.5);
}

game.setTime = function() {
  game.timer = setTimeout(function() {
    game.win();
  }, 30000);
}

game.populate = function(x, y, rows, cols) {
  var rows = rows;
  var cols = cols;

  var group = [];

  for (var i = 0; i < rows; i++) {
    for (var j = 0; j < cols; j++) {

      if (i === 0 && j === 0) continue;
      if (i === 0 && j === cols - 1) continue;
      if (i === rows - 1 && j === 0) continue;
      if (i === rows - 1 && j === cols - 1) continue;

      var random = game.rnd.integerInRange(1, 3);
      var human = this.game.add.sprite(
        (x + 20 * i), 
        (y + 25 * j), 
        'human' + random
      );
      human.anchor.setTo(0.5, 0.5);
      human.animations.add('go', Phaser.Animation.generateFrameNames('human' + random + '_', 1, 3, '.png', 3), 3, false, false); 
      group.push(human);
    }
  }

  crowd.push(group);
}

game.runFlags = function() {
  if(game.rnd.integerInRange(1, 100) >= 99) {
    var group = game.rnd.integerInRange(0, crowd.length - 1);
    var person = game.rnd.integerInRange(0, crowd[group].length - 1);

    if (crowd[group][person].has_nav_flag || crowd[group][person].mute) return;

    var type = game.rnd.integerInRange(0, 1);
    var elem;

    var health = 3;

    if (type) {
      elem = this.game.add.sprite(
        crowd[group][person].x + 83,
        crowd[group][person].y - 69,
        'flag'
      );
    } else {
      elem = this.game.add.sprite(
        crowd[group][person].x + 134,
        crowd[group][person].y - 100,
        'echo'
      ); 
    }
    climate += 50;
    elem.anchor.setTo(1, 0);
    elem.inputEnabled = true
    elem.input.useHandCursor = true;
    targets.push(elem);
    elem.events.onInputDown.add(function() {
      if (selected === 'helmet') {
        batonSd.play();
        var nav_flag = crowd[group].some(function(e) {
          return e.has_nav_flag === true;
        });
        elem.destroy()
        climate -= 50;        
      };
    });
  }
}

game.runNavFlag = function() {
  if (nav_flag) return;

  if(game.rnd.integerInRange(1, 100) >= 98) {
    var group = game.rnd.integerInRange(0, crowd.length - 1);
    var person = game.rnd.integerInRange(0, crowd[group].length - 1);
    
    if (crowd[group][person].has_nav_flag || crowd[group][person].mute) return;

    var x = crowd[group][person].x + 68;
    var y = crowd[group][person].y - 120;

    nav_flag = this.game.add.sprite(x, y, 'nav_flag');
    crowd[group][person].has_nav_flag = true;
    nav_flag.anchor.setTo(1, 0);
    nav_flag.inputEnabled = true
    nav_flag.input.useHandCursor = true;
    var that = this;
    nav_flag.events.onInputDown.add(function() {
      if (selected === 'omon') {
        if (squadGroup) return;
        omonSd.play();
        squadGroup = game.add.group();
        if (x > that.game.world.centerX) {
          for (var i = 0; i < 3; i++) {
            var squadMan =  squadGroup.create(
              that.game.world.width + 100, 
              that.game.world.centerY + (i * 20), 
              'omon_squad',
              'omon_001.png'
            );
            squadMan.animations.add('go', Phaser.Animation.generateFrameNames('omon_', 1, 2, '.png', 3), 4, true, false);

            squadMan.animations.play('go');
          }
        } else {
          for (var i = 0; i < 3; i++) {
            var squadMan =  squadGroup.create(
              -100, 
              that.game.world.centerY + (i * 40),
              'omon_squad2',
              'omon2_001.png'
            );
            squadMan.animations.add('go', Phaser.Animation.generateFrameNames('omon2_', 1, 2, '.png', 3), 4, true, false);

            squadMan.animations.play('go');
          }
        }

        game.physics.arcade.enable(nav_flag);
        squadGroup.children.forEach(function(s) {
          game.physics.arcade.enable(s);
          game.physics.arcade.moveToObject(s, nav_flag, 300); 
        })
      };
    });
    targets.push(nav_flag);

    nav_flag.kill_me = function() {
      crowd[group][person].has_nav_flag = false;
      nav_flag.destroy();
      squadGroup.removeAll(true);
      squadGroup = null;
      nav_flag = null;
    }
  }
}

game.runPeople = function() {
  var group = game.rnd.integerInRange(0, crowd.length - 1);
  var person = game.rnd.integerInRange(0, crowd[group].length - 1);
  var randPerson = crowd[group][person];
  randPerson.animations.play('go');
  randPerson.animations.refreshFrame();
}

game.runFires = function() {

  if(fireStick) return;

  if(game.rnd.integerInRange(1, 300) >= 299) {
    var group = game.rnd.integerInRange(0, crowd.length - 1);
    var person = game.rnd.integerInRange(0, crowd[group].length - 1);

    if (crowd[group][person].has_nav_flag || crowd[group][person].mute) return;

    var x = crowd[group][person].x - 10;
    var y = crowd[group][person].y - 50;

    fireHalo = this.game.add.sprite(x, y, 'fire_halo');
    fireHalo.anchor.setTo(0.5, 0.5);
    fireStick = this.game.add.sprite(x, y, 'fire');
    fireStick.anchor.setTo(0.5, 0);
    fireCloud = this.game.add.sprite(x, y - 20, 'fire_cloud');
    fireCloud.anchor.setTo(0.5, 0.5);

    climate += 150;
    fireStick.anchor.setTo(0.5, 0);
    fireStick.inputEnabled = true
    fireStick.input.useHandCursor = true;

    var that = this;

    fireHalo.animations.add('fire', Phaser.Animation.generateFrameNames('fire_halo_', 1, 2, '.png', 3), 10, true, false);
    fireStick.animations.add('fire', Phaser.Animation.generateFrameNames('fire_', 1, 2, '.png', 3), 10, true, false);
    fireCloud.animations.add('fire', Phaser.Animation.generateFrameNames('fire_cloud_', 1, 3, '.png', 3), 10, true, false);

    fireStick.animations.play('fire');
    fireHalo.animations.play('fire');
    fireCloud.animations.play('fire');

    fireSd.play()

    fireStick.events.onInputDown.add(function() {
      if (selected === 'water') {
        fireSd.stop();
        waterSd.play()
        watersplash = game.add.sprite(
          that.game.world.centerX, 
          that.game.world.height + 100,
          'waterdrop'
        );

        watersplash.anchor.setTo(0.5, 0.5);
        watersplash.scale.setTo(0.5);

        game.physics.arcade.enable(watersplash);
        game.physics.arcade.enable(fireStick);

        game.physics.arcade.moveToObject(watersplash, fireStick, 500); 
      };
    });
    targets.push(fireStick);
  }
}

game.runBars = function() {
  var curBar = Math.round(climate / 200);
  bars.forEach(function(b, i) {
    if(i < curBar){
      b.visible = true;
    } else {
      b.visible = false;
    }
  });
}

game.update = function() {
  game.runPeople();
  game.runFlags();
  game.runBars();
  game.runFires();
  game.runNavFlag();

  if (nav_flag && squadGroup) {
    this.game.physics.arcade.collide(
      nav_flag,
      squadGroup,
      function() {
        var fight = game.add.sprite(nav_flag.x, nav_flag.y, 'fight', 'fight/fight_001.png');
        fight.anchor.setTo(0.5, 0.5);
        var fightAnim = fight.animations.add('go', Phaser.Animation.generateFrameNames('fight_', 1, 3, '.png', 3), 10, false, false);
        fightAnim.onComplete.add(function() {
          fight.destroy();
          fight = null;
        }, this);
        fight.animations.play('go');
        nav_flag.kill_me();
      }, 
      null, 
      this
    ); 
  }

  if (watersplash && fireStick) {
    this.game.physics.arcade.overlap(
      watersplash,
      fireStick,
      function() {
        watersplash.destroy();
        fireStick.destroy();
        fireHalo.destroy();
        fireCloud.destroy();
        climate -= 150;
        watersplash = null;
        fireStick = null;
        fireHalo = null;
        fireCloud = null;
      }, 
      null, 
      this
    ); 
  }

  var text = timer <= 9 ? ('0' + timer) : timer;
  timerText.setText('00:' + text);

  if (climate > 1000) {
    game.loose();  
    targets = [];
    crowd = [];
    selected = 'helmet';
    climate = 0;
    targets = [];

    squad_target = null;
    nav_flag = null;
    squadGroup = null;

    fireStick = null;
    watersplash = null;
  }
}

module.exports = game;
