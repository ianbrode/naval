var game = {};

game.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'level2'
  );
  
  logo.anchor.setTo(0.5, 0.5);

  var that = this;
  setTimeout(function() {
    that.game.state.start('stage2');
  }, 1000)
};

module.exports = game;
