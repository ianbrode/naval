var game = {};

game.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'game'
  );
  
  logo.anchor.setTo(0.5, 0.5);
};

module.exports = game;
