var preloader = {};

preloader.preload = function () {
  this.game.load.image('vk', './images/vk.png');
  this.game.load.image('fb', './images/fb.png');
  this.game.load.image('od', './images/od.png');

  this.game.load.image('play_again_win', './images/play_again_lose.png');
  
  this.game.load.image('clock', './images/clock.png');

  // background
  this.game.load.image('menu', './images/start.png');
  this.game.load.image('win', './images/win.png');
  this.game.load.image('loose', './images/loose.png');
  this.game.load.image('game', './images/game.png');
  this.game.load.image('game2', './images/game2.png');

  // level logos
  this.game.load.image('level1', './images/levels/level_001.png');
  this.game.load.image('level2', './images/levels/level_002.png');
  this.game.load.image('level3', './images/levels/level_003.png');
  this.game.load.image('level4', './images/levels/level_004.png');  

  // icons
  // helmet
  this.game.load.image('helmet', './images/icons/helmet/helmet.png');  
  this.game.load.image('helmet_on', './images/icons/helmet/helmet_pick.png');  
  this.game.load.image('helmet_off', './images/icons/helmet/helmet_hidden.png');  

  // omon
  this.game.load.image('omon', './images/icons/omon/omon.png');  
  this.game.load.image('omon_on', './images/icons/omon/omon_pick.png');  
  this.game.load.image('omon_off', './images/icons/omon/omon_hide.png');  

  // water
  this.game.load.image('water', './images/icons/water/water.png');  
  this.game.load.image('water_on', './images/icons/water/water_pick.png');  
  this.game.load.image('water_off', './images/icons/water/water_hidden.png');  

  // gas
  this.game.load.image('gas', './images/icons/gas/gas.png');  
  this.game.load.image('gas_on', './images/icons/gas/gas_pick.png');  
  this.game.load.image('gas_off', './images/icons/gas/gas_hidden.png');  

  // people
  this.game.load.atlasJSONHash('human1', './images/people/human1.png', './images/people/human1.json');
  this.game.load.atlasJSONHash('human2', './images/people/human2.png', './images/people/human2.json');
  this.game.load.atlasJSONHash('human3', './images/people/human3.png', './images/people/human3.json');
  this.game.load.atlasJSONHash('human4', './images/people/human4.png', './images/people/human4.json');
  this.game.load.atlasJSONHash('omon_squad', './images/people/omon.png', './images/people/omon.json');
  this.game.load.atlasJSONHash('omon_squad2', './images/people/omon2.png', './images/people/omon2.json');

  // targets
  this.game.load.image('flag', './images/duck_flag.png');
  this.game.load.image('echo', './images/echo.png');
  this.game.load.image('nav_flag', './images/navalny_flag.png');
  this.game.load.image('navalny', './images/navalny.png');
  // bar
  this.game.load.image('bar', './images/progressbar.png');
  this.game.load.image('zak', './images/avtozak_001.png');
  this.game.load.image('aim', './images/aim.png');
  this.game.load.image('waterdrop', './images/waterdrop.png');  
  this.game.load.atlasJSONHash('fire', './images/fire.png', './images/fire.json');
  this.game.load.atlasJSONHash('fight', './images/fight.png', './images/fight.json');

  this.game.load.atlasJSONHash('clouds', './images/clouds.png', './images/clouds.json');  
  this.game.load.atlasJSONHash('health', './images/health.png', './images/health.json');
  this.game.load.atlasJSONHash('fire_halo', './images/fire_halo.png', './images/fire_halo.json');  
  this.game.load.atlasJSONHash('fire_cloud', './images/fire_cloud.png', './images/fire_cloud.json');  

  // Audio
  this.game.load.audio('omon', './audio/omon.mp3');
  this.game.load.audio('baton', './audio/baton.mp3');
  this.game.load.audio('baton2', './audio/baton2.mp3');
  this.game.load.audio('fire', './audio/fire.mp3');
  this.game.load.audio('game', './audio/game.mp3');
  this.game.load.audio('gas', './audio/gas.mp3');
  this.game.load.audio('lose', './audio/lose.mp3');
  this.game.load.audio('nav_hit', './audio/nav_hit.mp3');
  this.game.load.audio('nav_hit2', './audio/nav_hit2.mp3');
  this.game.load.audio('water', './audio/water.mp3');
  this.game.load.audio('win', './audio/win.mp3');

};

preloader.create = function () {
  this.game.state.start('menu');
};

module.exports = preloader;
