var loose = {};
var batonSd;
var baton2Sd;
var fireSd;
var gameSd;
var loseSd;
var navHitSd;
var navHit2Sd;
var waterSd;
var winSd;

loose.makeSounds = function() {
  batonSd   = loose.add.audio('baton');
  baton2Sd  = loose.add.audio('baton2');
  fireSd    = loose.add.audio('fire');
  gameSd    = loose.add.audio('game');
  gameSd.loop = true;
  loseSd    = loose.add.audio('lose');
  loseSd.loop = true;
  navHitSd  = loose.add.audio('nav_hit');
  navHit2Sd = loose.add.audio('nav_hit2');
  waterSd   = loose.add.audio('water');
  winSd     = loose.add.audio('win');
  winSd.loop = true;
}

loose.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'loose'
  );

  logo.anchor.setTo(0.5, 0.5);

  var button = this.game.add.sprite(
    120,
    450,
    'play_again_win'
  )
  button.width = 364;
  button.height = 150;
  button.inputEnabled = true
  button.input.useHandCursor = true;
  button.alpha = 0.01;
  button.events.onInputDown.add(loose.run, this);

// document.getElementById('fb_lose').click()

  var vk = this.game.add.sprite(
    200,
    610,
    'vk'
  );
  vk.inputEnabled = true
  vk.input.useHandCursor = true;
  vk.events.onInputDown.add(function() {
    document.getElementById('vk_lose').click()
  }, this);

  var fb = this.game.add.sprite(
    310,
    610,
    'fb'
  );
  fb.inputEnabled = true
  fb.input.useHandCursor = true;
  fb.events.onInputDown.add(function() {
    document.getElementById('fb_lose').click()
  }, this);

  var od = this.game.add.sprite(
    420,
    610,
    'od'
  );
  od.inputEnabled = true
  od.input.useHandCursor = true;
  od.events.onInputDown.add(function() {
    document.getElementById('ok_lose').click()
  }, this);

  loose.makeSounds();
  loseSd.play()
};

loose.run = function() {
  loseSd.stop();
  this.game.state.start('stage1_logo');
}


module.exports = loose;
