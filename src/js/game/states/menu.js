var menu = {};

menu.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'menu'
  );

  logo.anchor.setTo(0.5, 0.5);
  logo.inputEnabled = true;
  logo.events.onInputDown.add(menu.run, this);
};

menu.run = function() {
  this.game.state.start('stage1_logo');
}

module.exports = menu;
