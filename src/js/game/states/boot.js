var properties = require('../properties');
var boot = {};

boot.create = function () {
  this.game.sound.mute = properties.mute;
  this.game.state.start('preloader');
};

module.exports = boot;
