var win = {};
var batonSd;
var baton2Sd;
var fireSd;
var gameSd;
var loseSd;
var navHitSd;
var navHit2Sd;
var waterSd;
var winSd;

win.makeSounds = function() {
  batonSd   = win.add.audio('baton');
  baton2Sd  = win.add.audio('baton2');
  fireSd    = win.add.audio('fire');
  gameSd    = win.add.audio('game');
  gameSd.loop = true;
  loseSd    = win.add.audio('lose');
  loseSd.loop = true;
  navHitSd  = win.add.audio('nav_hit');
  navHit2Sd = win.add.audio('nav_hit2');
  waterSd   = win.add.audio('water');
  winSd     = win.add.audio('win');
  winSd.loop = true;
}

var button;

win.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'win'
  );
  win.makeSounds();
  logo.anchor.setTo(0.5, 0.5);

  var button = this.game.add.sprite(
    120,
    450,
    'play_again_win'
  )
  button.width = 364;
  button.height = 150;
  button.inputEnabled = true
  button.input.useHandCursor = true;
  button.alpha = 0.01;
  button.events.onInputDown.add(win.run, this);

  // document.getElementById('fb_win').click()

  var vk = this.game.add.sprite(
    150,
    610,
    'vk'
  );
  vk.inputEnabled = true
  vk.input.useHandCursor = true;
  vk.events.onInputDown.add(function() {
    document.getElementById('vk_win').click()
  }, this);

  var fb = this.game.add.sprite(
    260,
    610,
    'fb'
  );
  fb.inputEnabled = true
  fb.input.useHandCursor = true;
  fb.events.onInputDown.add(function() {
    document.getElementById('fb_win').click()
  }, this);

  var od = this.game.add.sprite(
    370,
    610,
    'od'
  );
  od.inputEnabled = true
  od.input.useHandCursor = true;
  od.events.onInputDown.add(function() {
    document.getElementById('ok_win').click()
  }, this);

  winSd.play()
};

win.run = function() {
  winSd.stop()
  this.game.state.start('stage1_logo');
}
module.exports = win;
