var game = {};
var crowd = [];
var targets = [];
var selected = 'helmet';
var climate = 0;
var bars = [];

var interval;
var timer = 30;
var timerText;

var batonSd;
var baton2Sd;
var fireSd;
var gameSd;
var loseSd;
var navHitSd;
var navHit2Sd;
var waterSd;
var winSd;

game.makeSounds = function() {
  batonSd   = game.add.audio('baton');
  baton2Sd  = game.add.audio('baton2');
  fireSd    = game.add.audio('fire');
  gameSd    = game.add.audio('game');
  gameSd.loop = true;
  loseSd    = game.add.audio('lose');
  loseSd.loop = true;
  navHitSd  = game.add.audio('nav_hit');
  navHit2Sd = game.add.audio('nav_hit2');
  waterSd   = game.add.audio('water');
  winSd     = game.add.audio('win');
  winSd.loop = true;
}

game.makeInterval = function() {
  interval = setInterval(function() {
    timer = timer - 1;
  }, 1000);
}

game.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.makeSounds();
  var background = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'game2'
  );
  background.anchor.setTo(0.5, 0.5);

  game.interface();
  game.populate(200, 200, 3, 3, true);
  game.populate(430, 430, 3, 3, true);
  game.populate(954, 300, 3, 3, true);
  game.setTime();
  game.makeInterval();

  var clock = this.game.add.sprite(this.game.world.width - 100, 30, 'clock');

  var bar = this.game.add.sprite(20, 10, 'bar');
  game.generateBar();
  gameSd.play();

  timerText = game.add.text(this.game.world.width - 180, 40, ('00:' + timer), {
    font: "24px Arial",
    fill: "#ff0044",
    align: "center"
  });
};

game.win = function() {
  targets.forEach(function(t) {
    t.destroy();
  });
  crowd.forEach(function(p) {p.mute = true;});
  var that = this;
  setTimeout(function() {
    gameSd.stop();
    that.game.state.start('stage2_logo');
  }, 2000);
  clearInterval(interval);
}

game.loose = function() {
  clearTimeout(game.timer);
  clearInterval(interval);
  gameSd.stop();
  this.game.state.start('loose');
}

game.interface = function() {
  var helmet = this.game.add.sprite(450, 640, 'helmet_on');
  helmet.anchor.setTo(0.5, 0.5);
  helmet.inputEnabled = true
  helmet.input.useHandCursor = true;

  var omon = this.game.add.sprite(610, 640, 'omon_off');
  omon.anchor.setTo(0.5, 0.5);

  var water = this.game.add.sprite(740, 640, 'water_off');
  water.anchor.setTo(0.5, 0.5);

  var gas = this.game.add.sprite(850, 640, 'gas_off');
  gas.anchor.setTo(0.5, 0.5);
}

game.populate = function(x, y, rows, cols, addon) {
  var rows = rows;
  var cols = cols;

  if (addon) {
    rows += 2;
    cols += 2;
  }

  for (var i = 0; i < rows; i++) {
    for (var j = 0; j < cols; j++) {
      if (addon) {
        if (i === 0 && j === 0) continue;
        if (i === 0 && j === cols - 1) continue;
        if (i === rows - 1 && j === 0) continue;
        if (i === rows - 1 && j === cols - 1) continue;
      }

      var random = game.rnd.integerInRange(1, 3);
      var human = this.game.add.sprite(
        (x + 20 * i), 
        (y + 25 * j), 
        'human' + random
      );
      human.anchor.setTo(0.5, 0.5);
      human.animations.add('go', Phaser.Animation.generateFrameNames('human' + random + '_', 1, 3, '.png', 3), 3, false, false);     
      crowd.push(human);
    }
  }
}

game.setTime = function() {
  game.timer = setTimeout(function() {
    game.win();
  }, 30000);
}

game.generateBar = function() {
  var base = { x: 60, y: 53 };

  for (var i = 0; i < 5; i++) {
    var bar = this.game.add.sprite(
      base.x + (25 * i),
      base.y,
      'health',
      'progressbar_point_00' + (i + 1) + '.png'
    );
    bars.push(bar)
  }
}

game.runBars = function() {
  var curBar = Math.round(climate / 100);
  bars.forEach(function(b, i) {
    if(i < curBar){
      b.visible = true;
    } else {
      b.visible = false;
    }
  });
}

game.runFlags = function() {
  if (game.rnd.integerInRange(1, 100) >= 99) {
    var person = game.rnd.integerInRange(0, crowd.length - 1);

    if (crowd[person].mute) return;

    var type = game.rnd.integerInRange(0, 1);
    var elem;

    if (type) {
      elem = this.game.add.sprite(
        crowd[person].x + 83,
        crowd[person].y - 69,
        'flag'
      );
    } else {
      elem = this.game.add.sprite(
        crowd[person].x + 134,
        crowd[person].y - 100,
        'echo'
      );
    }
    climate += 50;
    elem.anchor.setTo(1, 0);
    elem.inputEnabled = true
    elem.input.useHandCursor = true;
    targets.push(elem);
    elem.events.onInputDown.add(function() {
      if (selected === 'helmet') {
        batonSd.play();
        elem.destroy();
        climate -= 50;
      };
    });
  }
}

game.runPeople = function() {
  var randPerson = crowd[game.rnd.integerInRange(0, crowd.length - 1)]
  randPerson.animations.play('go');
  randPerson.animations.refreshFrame();
}

game.update = function() {
  game.runPeople();
  game.runFlags();
  game.runBars();

  var text = timer <= 9 ? ('0' + timer) : timer;
  timerText.setText('00:' + text);

  if (climate >= 500) {
    game.loose();  
    crowd = [];
    targets = [];
    selected = 'helmet';
    climate = 0;
    timer = 30;
  }
}

module.exports = game;
