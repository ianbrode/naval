var game = {};
var crowd = [];
var selected = 'helmet';
var climate = 0;
var bars = [];
var targets = [];

var squad_target;
var nav_flag;
var squadGroup;

var fireCloud;
var fireHalo;
var fireStick;
var watersplash;

var zaks = [];

var movers = [];

var gas_started = false;

var navTimer;
var nav;

var batonSd;
var baton2Sd;
var fireSd;
var gameSd;
var loseSd;
var navHitSd;
var navHit2Sd;
var waterSd;
var winSd;
var gasSd;

var interval;
var timer = 90;
var timerText;

game.makeInterval = function() {
  interval = setInterval(function() {
    timer = timer - 1;
  }, 1000);
}

game.makeSounds = function() {
  batonSd   = game.add.audio('baton');
  baton2Sd  = game.add.audio('baton2');
  fireSd    = game.add.audio('fire');
  gameSd    = game.add.audio('game');
  gameSd.loop = true;
  loseSd    = game.add.audio('lose');
  loseSd.loop = true;
  navHitSd  = game.add.audio('nav_hit');
  navHit2Sd = game.add.audio('nav_hit2');
  waterSd   = game.add.audio('water');
  winSd     = game.add.audio('win');
  winSd.loop = true;
  gasSd = game.add.audio('gas');
}

game.makeZak = function(x, y) {
  var zak = this.game.add.sprite(x, y, 'zak');
  zak.anchor.setTo(0.5, 0.5);
  zak.scale.setTo(0.5);
  game.physics.arcade.enable(zak);
  zaks.push(zak);
}

game.create = function () {
  this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.game.physics.startSystem(Phaser.Physics.ARCADE);

  var logo = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'game2'
  );
  
  logo.anchor.setTo(0.5, 0.5);

  game.makeZak(54, 202);
  game.makeZak(379, 9);
  game.makeZak(1231, 89);
  game.makeZak(1043, 656);
  game.makeZak(200, 658);

  game.populate(200, 200, 7, 7);
  game.populate(200, 400, 5, 5);

  game.populate(430, 400, 7, 7);
  game.populate(630, 400, 5, 5);
  game.populate(840, 380, 7, 7);

  game.populate(1046, 236, 7, 7);

  game.interface();
  game.setTime();

  game.makeInterval();
  var clock = this.game.add.sprite(this.game.world.width - 100, 30, 'clock');

  var minutes = Math.floor(timer / 60);
  var seconds = (timer % 60);
  timerText = game.add.text(this.game.world.width - 180, 40, ('0' + minutes + ':' + seconds), {
    font: "24px Arial",
    fill: "#ff0044",
    align: "center"
  });

  game.makeSounds();
  gameSd.play();
  var bar = this.game.add.sprite(20, 10, 'bar');
  game.generateBar();
};

game.generateBar = function() {
  var base = { x: 60, y: 53 };

  for (var i = 0; i < 5; i++) {
    var bar = this.game.add.sprite(
      base.x + (25 * i),
      base.y,
      'health',
      'progressbar_point_00' + (i + 1) + '.png'
    );
    bars.push(bar)
  }
}

game.win = function() {
  targets.forEach(function(t) {
    t.destroy();
  });
  squadGroup && squadGroup.removeAll(true);
  crowd.forEach(function(group) {
    group.forEach(function(p) {
      p.mute = true;
    })
  });
  var that = this;
  setTimeout(function() {
    gameSd.stop();
    that.game.state.start('win');
  }, 2000)
  nav.destroy();
  clearTimeout(navTimer);
  clearTimeout(game.timer);
}

game.loose = function() {
  gameSd.stop();
  clearTimeout(game.timer);
  clearTimeout(navTimer);
  timer = 90;
  this.game.state.start('loose');
}

game.interface = function() {
  var helmet = this.game.add.sprite(450, 640, 'helmet_on');
  helmet.anchor.setTo(0.5, 0.5);
  helmet.inputEnabled = true
  helmet.input.useHandCursor = true;
  helmet.events.onInputDown.add(function() {
    selected = 'helmet';
    helmet.loadTexture('helmet_on', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water', 0);
    gas.loadTexture('gas', 0);
  }, this);

  var q = game.input.keyboard.addKey(Phaser.Keyboard.Q);
  q.onDown.add(function() {
    selected = 'helmet';
    helmet.loadTexture('helmet_on', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water', 0);
    gas.loadTexture('gas', 0);
  }, this);

  var omon = this.game.add.sprite(610, 640, 'omon');
  omon.anchor.setTo(0.5, 0.5);
  omon.inputEnabled = true
  omon.input.useHandCursor = true;
  omon.events.onInputDown.add(function() {
    selected = 'omon';
    omon.loadTexture('omon_on', 0);
    helmet.loadTexture('helmet', 0);
    water.loadTexture('water', 0);
  }, this);

  var w = game.input.keyboard.addKey(Phaser.Keyboard.W);
  w.onDown.add(function() {
    selected = 'omon';
    omon.loadTexture('omon_on', 0);
    helmet.loadTexture('helmet', 0);
    water.loadTexture('water', 0);
  }, this);

  var water = this.game.add.sprite(740, 640, 'water');
  water.anchor.setTo(0.5, 0.5);
  water.inputEnabled = true
  water.input.useHandCursor = true;
  water.events.onInputDown.add(function() {
    selected = 'water';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water_on', 0);
  }, this);

  var e = game.input.keyboard.addKey(Phaser.Keyboard.E);
  e.onDown.add(function() {
    selected = 'water';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water_on', 0);
  }, this);

  var gas = this.game.add.sprite(
    this.game.world.centerX, 
    this.game.world.centerY, 
    'gas'
  );
  gas.anchor.setTo(0.5, 0.5);
  gas.inputEnabled = true
  gas.input.useHandCursor = true;
  gas.events.onInputDown.add(function() {
    selected = 'gas';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water', 0);
    gas.loadTexture('gas_on', 0);
  }, this);
  game.add.tween(gas).to( { x: 850, y: 640 }, 1500, Phaser.Easing.Bounce.Out, true);

  var r = game.input.keyboard.addKey(Phaser.Keyboard.R);
  r.onDown.add(function() {
    selected = 'gas';
    helmet.loadTexture('helmet', 0);
    omon.loadTexture('omon', 0);
    water.loadTexture('water', 0);
    gas.loadTexture('gas_on', 0);
  }, this);
}

game.setTime = function() {
  game.timer = setTimeout(function() {
    game.lose();
  }, 60000);
}

game.populate = function(x, y, rows, cols) {
  var rows = rows;
  var cols = cols;

  var group = [];

  for (var i = 0; i < rows; i++) {
    for (var j = 0; j < cols; j++) {

      if (i === 0 && j === 0) continue;
      if (i === 0 && j === cols - 1) continue;
      if (i === rows - 1 && j === 0) continue;
      if (i === rows - 1 && j === cols - 1) continue;

      var random = game.rnd.integerInRange(1, 3);
      var human = this.game.add.sprite(
        (x + 20 * i), 
        (y + 25 * j), 
        'human' + random
      );
      human.anchor.setTo(0.5, 0.5);
      human.animations.add('go', Phaser.Animation.generateFrameNames('human' + random + '_', 1, 3, '.png', 3), 3, false, false); 

      human.inputEnabled = true;
      var that = this;
      human.events.onInputDown.add(function() {
        if (selected === 'gas') {
          gasSd.play();
          if(gas_started === false) {
            gas_started = true;
            crowd.forEach(function(group) {
              group.forEach(function(p) {
                p.mute = true;
              })
            });
            game.placeNav();
          }
          
          var cloud = that.game.add.sprite(
            human.x - 100,
            human.y - 100,
            'clouds'
          )
          cloud.animations.add('go', Phaser.Animation.generateFrameNames('cloud_', 1, 5, '.png', 3), 3, true, true); 
          cloud.animations.play('go');
          cloud.anchor.setTo(0.5, 0);
          cloud.alpha = 0.5;

          group.forEach(function(h) {
            h.gassed = true;
            game.physics.arcade.enable(h);

            var dist = 0;
            var targetZak = null;
            zaks.forEach(function(z){
              var distToZak = game.physics.arcade.distanceBetween(z, h);
              if (dist === 0 || distToZak < dist) {
                dist = distToZak;
                targetZak = z;
              } 
            })
            game.physics.arcade.moveToObject(h, targetZak, 5);
            movers.push([h, targetZak]);
          });

          targets.forEach(function(t){ t.destroy(); });
          targets = [];
        }
      });

      group.push(human);
    }
  }

  crowd.push(group);
}

game.runFlags = function() {
  if(game.rnd.integerInRange(1, 100) >= 99) {
    var group = game.rnd.integerInRange(0, crowd.length - 1);
    var person = game.rnd.integerInRange(0, crowd[group].length - 1);

    if (crowd[group][person].has_nav_flag || crowd[group][person].mute) return;

    var type = game.rnd.integerInRange(0, 1);
    var elem;

    var health = 3;

    if (type) {
      elem = this.game.add.sprite(
        crowd[group][person].x + 83,
        crowd[group][person].y - 69,
        'flag'
      );
    } else {
      elem = this.game.add.sprite(
        crowd[group][person].x + 134,
        crowd[group][person].y - 100,
        'echo'
      ); 
    }
    climate += 50;
    elem.anchor.setTo(1, 0);
    elem.inputEnabled = true
    elem.input.useHandCursor = true;
    targets.push(elem);
    elem.events.onInputDown.add(function() {
      if (selected === 'helmet') {
        batonSd.play();
        var nav_flag = crowd[group].some(function(e) {
          return e.has_nav_flag === true;
        });
        elem.destroy()
        climate -= 50;      
      };
    });
  }
}

game.runPeople = function() {
  var group = game.rnd.integerInRange(0, crowd.length - 1);
  var person = game.rnd.integerInRange(0, crowd[group].length - 1);
  var randPerson = crowd[group][person];
  randPerson.animations.play('go');
  randPerson.animations.refreshFrame();
}

game.runBars = function() {
  var curBar = Math.round(climate / 200);
  bars.forEach(function(b, i) {
    if(i < curBar){
      b.visible = true;
    } else {
      b.visible = false;
    }
  });
}

var counter = 0;

game.placeNav = function() {
  var x = game.rnd.integerInRange(148, 1050);
  var y = game.rnd.integerInRange(150, 500);
  nav = this.game.add.sprite(x, y, 'navalny');
  nav.anchor.setTo(0.5, 0.5);
  nav.inputEnabled = true
  nav.input.useHandCursor = true;

  navTimer = setTimeout(function() {
    nav.destroy();
    game.placeNav();              
  }, 700)

  nav.events.onInputDown.add(function() {

    navHitSd.play()

    counter += 1;
    if (counter < 5) {
      clearTimeout(navTimer);
      nav.tint = 0xff0000;
      setTimeout(function() {
        nav.destroy();
        game.placeNav();
      }, 300)
    } else {
      clearTimeout(navTimer);
      nav.destroy();
      setTimeout(function() { game.win(); }, 5000)
    }
  })
}

game.update = function() {
  game.runPeople();
  game.runFlags();
  game.runBars();
  game.setTime();

  var that = this;

  if (movers.length > 0 && gas_started) {
    movers.forEach(function(m, i) {
      that.game.physics.arcade.overlap(
        m[0],
        m[1],
        function() {
          m[0].destroy();
          m = null;
          movers.splice(i, 1);
        },
        null, 
        this
      );
    });
  }
  var minutes = Math.floor(timer / 60);
  var seconds = (timer % 60);

  var text = seconds <= 9 ? ('0' + seconds) : seconds;
  timerText.setText('0' + minutes + ':' + text);

  if (climate > 1000) {
    game.loose();  
    targets = [];
    crowd = [];
    selected = 'helmet';
    climate = 0;
    targets = [];
    zaks = [];

    movers = [];

    gas_started = false;

    squad_target = null;
    nav_flag = null;
    squadGroup = null;

    fireStick = null;
    watersplash = null;
  }
}

module.exports = game;
