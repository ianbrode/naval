var _ = require('lodash');
var properties = require('./properties');

var states = {
  boot: require('./states/boot.js'),
  preloader: require('./states/preloader.js'),
  stage1: require('./states/stage-1.js'),
  stage2: require('./states/stage-2.js'),
  stage3: require('./states/stage-3.js'),
  stage4: require('./states/stage-4.js'),

  stage1_logo: require('./states/stage-1-logo.js'),
  stage2_logo: require('./states/stage-2-logo.js'),
  stage3_logo: require('./states/stage-3-logo.js'),
  stage4_logo: require('./states/stage-4-logo.js'),

  win: require('./states/win.js'),
  loose: require('./states/loose.js'),
  menu: require('./states/menu.js'),
};

var game = new Phaser.Game(properties.size.x, properties.size.y, Phaser.AUTO, 'game');

// Automatically register each state.
_.each(states, function(state, key) {
  game.state.add(key, state);
});

game.state.start('boot');
